use std::path::PathBuf;

use fern::colors::{Color, ColoredLevelConfig};

mod rotation;

fn get_log_path() -> PathBuf {
    let dir = if cfg!(target_os = "macos") {
        dirs::home_dir()
            .unwrap()
            .join("Library")
            .join("Logs")
            .join("OkunaDesktop")
    } else {
        dirs::data_local_dir().unwrap().join("okuna").join("logs")
    };
    std::fs::create_dir_all(&dir).expect("Failed to create log dir");
    dir
}

pub fn setup_logging(verbosity: u8, log_to_file: bool) -> Result<(), fern::InitError> {
    let colors = ColoredLevelConfig::new()
        .error(Color::Red)
        .warn(Color::Yellow)
        .info(Color::Green)
        .debug(Color::White)
        .trace(Color::BrightBlack);
    let mut logger = fern::Dispatch::new();
    logger = match verbosity {
        0 => logger.level(log::LevelFilter::Warn),
        1 => logger.level(log::LevelFilter::Info),
        2 => logger.level(log::LevelFilter::Debug),
        _ => logger.level(log::LevelFilter::Trace),
    };

    let stdout_logger = fern::Dispatch::new()
        .format(move |out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}][{}] {}\x1B[0m",
                format_args!("\x1B[{}m", colors.get_color(&record.level()).to_fg_str()),
                chrono::Local::now().format("%H:%M:%S"),
                record.level(),
                record.target(),
                message
            ))
        })
        .chain(std::io::stdout());

    let mut old_logs = Vec::new();

    if log_to_file {
        let log_dir = get_log_path();
        old_logs = rotation::get_old_log_files(&log_dir);
        let log_file_latest = log_dir.join("latest.log");
        let log_file_time = log_dir.join(
            format_args!("{}.log", chrono::Local::now().format("%Y.%m.%d.%H.%M.%S")).to_string(),
        );
        logger = logger.chain(
            fern::Dispatch::new()
                .format(|out, message, record| {
                    out.finish(format_args!(
                        "[{}][{}][{}] {}",
                        chrono::Local::now().format("%Y-%m-%d %H:%M:%S"),
                        record.level(),
                        record.target(),
                        message
                    ))
                })
                .chain(
                    std::fs::OpenOptions::new()
                        .truncate(true)
                        .write(true)
                        .create(true)
                        .open(log_file_latest)?,
                )
                .chain(fern::log_file(log_file_time)?),
        );
    }
    logger.chain(stdout_logger).apply()?;

    // do this after the logger has been applied so the logger can be used
    if !old_logs.is_empty() {
        rotation::clean_up_old_logs(old_logs);
    }
    Ok(())
}
