use std::{
    env,
    io::Error as IOError,
    path::PathBuf,
    process::{Command, Output},
};

use log::{debug, error, info};

use lazy_static::lazy_static;

lazy_static! {
    pub static ref FFMPEG: FFmpeg = FFmpeg::new();
}

pub struct FFmpeg {
    ffmpeg_path: PathBuf,
    ffmpeg_binary: PathBuf,
}

impl FFmpeg {
    fn new() -> Self {
        let ffmpeg_path = env::current_exe().unwrap().parent().unwrap().to_owned();
        let ffmpeg_binary = if cfg!(target_os = "windows") {
            ffmpeg_path.join("ffmpeg.exe")
        } else {
            ffmpeg_path.join("ffmpeg")
        };
        Self {
            ffmpeg_path,
            ffmpeg_binary,
        }
    }

    pub fn run_ffmpeg<I, S>(&self, args: I) -> Result<Output, IOError>
    where
        I: IntoIterator<Item = S>,
        S: AsRef<str>,
    {
        let mut args: Vec<String> = args.into_iter().map(|s| s.as_ref().into()).collect();
        if cfg!(target_os = "windows") {
            // FFmpeg on Windows doesn't seem to handle paths using a forward slash as separator, so
            // replace them with backslashes. As a forward slash may be used in calc expressions
            // though, we need to make sure we only replace in input files (-i) and the output file
            // (last arg).
            let mut input = false;
            let last_idx = args.len() - 1;
            for (i, arg) in args.iter_mut().enumerate() {
                if arg.as_str() == "-i" {
                    input = true;
                    continue;
                }
                if input || i == last_idx {
                    *arg = arg.replace('/', "\\");
                    input = false;
                }
            }
        }
        info!("running ffmpeg with args: {}", args.join(" "));
        let output = Command::new(&self.ffmpeg_binary)
            .current_dir(&self.ffmpeg_path)
            .args(args)
            .output()?;
        let stdout = String::from_utf8_lossy(&output.stdout);
        let stderr = String::from_utf8_lossy(&output.stderr);
        if output.status.success() {
            debug!(
                "ffmpeg exited successfully\n--- stdout:\n{}\n--- stderr:\n{}",
                stdout, stderr
            );
        } else {
            let error = output
                .status
                .code()
                .map_or("SIGNAL".into(), |code| code.to_string());
            error!(
                "ffmpeg exited with error {}\n--- stdout:\n{}\n--- stderr:\n{}",
                error, stdout, stderr
            );
        }
        Ok(output)
    }
}
