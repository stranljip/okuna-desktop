use flutter_engine::plugins::prelude::*;
use log::debug;

pub const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "get_version";

pub struct GetVersionPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler;

impl Plugin for GetVersionPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl GetVersionPlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler)),
        }
    }
}

impl Handler {
    fn get_platform_version(&self) -> Result<Value, MethodCallError> {
        Ok(Value::String(whoami::os()))
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );
        match call.method.as_str() {
            "getPlatformVersion" => self.get_platform_version(),
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
