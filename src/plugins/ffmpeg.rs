use flutter_engine::plugins::prelude::*;
use log::debug;

use crate::util::ffmpeg::FFMPEG;

const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "flutter_ffmpeg";

pub struct FFmpegPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler;

impl Plugin for FFmpegPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl FFmpegPlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler)),
        }
    }
}

impl Handler {
    fn execute_with_arguments(
        &self,
        args: ExecuteWithArgumentsArgs,
    ) -> Result<Value, MethodCallError> {
        let output = FFMPEG
            .run_ffmpeg(&args.arguments)
            .map_err(MethodCallError::from_error)?;
        let code = output
            .status
            .code()
            .ok_or(MethodCallError::UnspecifiedError)?;
        Ok(json_value!({ "rc": code }))
    }

    fn execute(&self, args: ExecuteArgs) -> Result<Value, MethodCallError> {
        let delimiter = args.delimiter.as_ref().map_or(" ", |d| d.as_str());
        let arguments = args.command.split(delimiter);
        let output = FFMPEG
            .run_ffmpeg(arguments)
            .map_err(MethodCallError::from_error)?;
        let code = output
            .status
            .code()
            .ok_or(MethodCallError::UnspecifiedError)?;
        Ok(json_value!({ "rc": code }))
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );

        match call.method.as_str() {
            "getPlatform" => Ok(json_value!({
                "platform": "desktop"
            })),
            "execute" => {
                let args = from_value(&call.args)?;
                self.execute(args)
            }
            "executeWithArguments" => {
                let args = from_value(&call.args)?;
                self.execute_with_arguments(args)
            }
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}

#[derive(Serialize, Deserialize)]
struct ExecuteWithArgumentsArgs {
    arguments: Vec<String>,
}

#[derive(Serialize, Deserialize)]
struct ExecuteArgs {
    command: String,
    delimiter: Option<String>,
}
