use flutter_engine::plugins::prelude::*;
use image::GenericImageView;
use log::debug;
use rexif::ExifData;
use serde::{Deserialize, Serialize};

mod args;

const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "flutter_image_compress";

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
enum TargetFormat {
    JPEG,
    PNG,
}

pub struct ImageCompressPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler;

impl Plugin for ImageCompressPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl ImageCompressPlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler)),
        }
    }
}

impl Handler {
    fn get_exif_rotation_degrees(exif_data: &Option<ExifData>) -> Option<(i32, bool)> {
        if let Some(exif_data) = exif_data {
            let entry = exif_data
                .entries
                .iter()
                .find(|entry| entry.tag == rexif::ExifTag::Orientation);
            if let Some(entry) = entry {
                let rotation = entry.value.to_i64(0).unwrap_or(0) as i32;
                // flip means mirrored along the vertical axis before rotation
                // rotation is measured clockwise

                // rotation without flipping: 1, 8, 3, 6
                // rotation with flipping: 2, 7, 4, 5
                let (degrees, flip) = match rotation {
                    1 => (0, false),
                    2 => (0, true),
                    3 => (180, false),
                    4 => (180, true),
                    5 => (270, true),
                    6 => (270, false),
                    7 => (90, true),
                    8 => (90, false),
                    _ => unreachable!(),
                };
                return Some((degrees, flip));
            }
        }
        None
    }

    fn compress<A: args::CompressArgs>(&self, args: A) -> Result<Value, MethodCallError> {
        let input_image = args.input_image().map_err(MethodCallError::from_error)?;
        let exif_data = args.exif_data().ok();

        let (exif_rotation, flip) = if args.auto_correction_angle() {
            Self::get_exif_rotation_degrees(&exif_data).unwrap_or((0, false))
        } else {
            (0, false)
        };
        let (min_w, min_h) = if exif_rotation == 270 || exif_rotation == 90 {
            (args.min_height() as f32, args.min_width() as f32)
        } else {
            (args.min_width() as f32, args.min_height() as f32)
        };
        let src_w = input_image.width() as f32;
        let src_h = input_image.height() as f32;
        let scale = 1.0_f32.min((min_w / src_w).max(min_h / src_h));
        let (w, h) = (src_w * scale, src_h * scale);

        let mut img = input_image.resize(w as u32, h as u32, image::FilterType::Lanczos3);
        if flip {
            img = img.flipv();
        }
        let rotation = exif_rotation + args.rotate();
        match rotation % 360 {
            0 => (),
            90 => img = img.rotate90(),
            180 => img = img.rotate180(),
            270 => img = img.rotate270(),
            d => {
                return Err(MethodCallError::CustomError {
                    code: "invalid_rotation".into(),
                    message: format!("Rotation by {} degrees not supported", d),
                    details: Value::Null,
                })
            }
        }

        let result = args.write(&img).map_err(|err| match err {
            args::WriteError::ImageError(err) => MethodCallError::from_error(err),
            args::WriteError::IOError(err) => MethodCallError::from_error(err),
        })?;

        Ok(result)
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );

        match call.method.as_str() {
            "showLog" => Err(MethodCallError::NotImplemented),
            "compressWithList" => {
                let args = from_value::<args::CompressListArgs>(&call.args)?;
                self.compress(args)
            }
            "compressWithFile" => {
                let args = from_value::<args::CompressFileArgs>(&call.args)?;
                self.compress(args)
            }
            "compressWithFileAndGetFile" => {
                let args = from_value::<args::CompressFileToFileArgs>(&call.args)?;
                self.compress(args)
            }
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
