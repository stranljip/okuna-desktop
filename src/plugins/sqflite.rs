use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::Mutex;

use crate::util::paths;

use flutter_engine::plugins::prelude::*;
use log::{debug, warn};
use sqlite::Connection;

pub const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "com.tekartik.sqflite";

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct OpenDatabaseArgs {
    path: String,
    read_only: Option<bool>,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct CloseDatabaseArgs {
    id: i32,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ExecuteArgs {
    id: i32,
    sql: String,
    args: Option<Vec<Value>>,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct QueryArgs {
    id: i32,
    sql: String,
    args: Option<Vec<Value>>,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct UpdateArgs {
    id: i32,
    sql: String,
    args: Option<Vec<Value>>,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct InsertArgs {
    id: i32,
    sql: String,
    args: Option<Vec<Value>>,
}

pub struct SqflitePlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler {
    open_databases: HashMap<i32, Mutex<Connection>>,
    current_database_id: i32,
}

impl Plugin for SqflitePlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl SqflitePlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler {
                open_databases: HashMap::new(),
                current_database_id: 0,
            })),
        }
    }
}

impl Handler {
    fn get_databases_path(&self) -> Result<Value, MethodCallError> {
        match paths::get_data_dir() {
            Some(dir) => Ok(Value::String(dir.to_string_lossy().into_owned())),
            None => Err(MethodCallError::UnspecifiedError),
        }
    }

    fn open_database(&mut self, args: OpenDatabaseArgs) -> Result<Value, MethodCallError> {
        let path = PathBuf::from(args.path);
        let db = sqlite::Connection::open_with_flags(
            path,
            match args.read_only {
                Some(true) => sqlite::OpenFlags::new().set_read_only(),
                _ => sqlite::OpenFlags::new().set_read_write().set_create(),
            },
        )
        .map_err(MethodCallError::from_error)?;

        let id = self.current_database_id;
        self.current_database_id += 1;
        self.open_databases.insert(id, Mutex::new(db));
        Ok(Value::I32(id))
    }

    fn close_database(&mut self, args: CloseDatabaseArgs) -> Result<Value, MethodCallError> {
        self.open_databases.remove(&args.id);
        Ok(Value::Null)
    }

    fn execute(&self, args: ExecuteArgs) -> Result<Value, MethodCallError> {
        let db = match self.open_databases.get(&args.id) {
            Some(db) => db,
            _ => return Err(MethodCallError::UnspecifiedError),
        };
        let db = db.lock().unwrap();

        let mut statement = db.prepare(&args.sql).map_err(MethodCallError::from_error)?;
        if let Some(sql_args) = args.args {
            bind_values_to_statement(&mut statement, &sql_args)
                .map_err(MethodCallError::from_error)?;
        }
        while let sqlite::State::Row = statement.next().map_err(MethodCallError::from_error)? {}
        Ok(Value::Null)
    }

    fn query(&self, args: QueryArgs) -> Result<Value, MethodCallError> {
        let db = match self.open_databases.get(&args.id) {
            Some(db) => db,
            _ => return Err(MethodCallError::UnspecifiedError),
        };
        let db = db.lock().unwrap();

        let mut statement = db.prepare(&args.sql).map_err(MethodCallError::from_error)?;
        if let Some(sql_args) = args.args {
            bind_values_to_statement(&mut statement, &sql_args)
                .map_err(MethodCallError::from_error)?;
        }

        let column_count = statement.count();
        let column_names = statement
            .names()
            .into_iter()
            .map(|s| Value::String(String::from(s)))
            .collect();

        //TODO: implement options call and returning a list of maps instead of a map with a list of lists

        let mut results_map = HashMap::new();
        results_map.insert("columns".into(), Value::List(column_names));
        let mut result_rows = Vec::new();

        while let sqlite::State::Row = statement.next().map_err(MethodCallError::from_error)? {
            result_rows.push(Value::List(
                statement_row_to_vec(&statement, column_count)
                    .map_err(MethodCallError::from_error)?,
            ));
        }
        results_map.insert("rows".into(), Value::List(result_rows));

        Ok(Value::Map(results_map))
    }

    fn update(&mut self, args: UpdateArgs) -> Result<Value, MethodCallError> {
        self.execute(ExecuteArgs {
            id: args.id,
            sql: args.sql.clone(),
            args: args.args,
        })?;

        let db = match self.open_databases.get(&args.id) {
            Some(db) => db,
            _ => return Err(MethodCallError::UnspecifiedError),
        };
        let db = db.lock().unwrap();

        let mut statement = db
            .prepare("SELECT changes()")
            .map_err(MethodCallError::from_error)?;
        match statement.next() {
            Ok(sqlite::State::Row) => match statement.read::<i64>(0) {
                Ok(changes) => Ok(Value::I64(changes)),
                _ => {
                    warn!("Reading changes for update didn't return an integer");
                    Ok(Value::Null)
                }
            },
            _ => {
                warn!("Failed to read changes for update");
                Ok(Value::Null)
            }
        }
    }

    fn insert(&mut self, args: InsertArgs) -> Result<Value, MethodCallError> {
        self.execute(ExecuteArgs {
            id: args.id,
            sql: args.sql.clone(),
            args: args.args,
        })?;

        let db = match self.open_databases.get(&args.id) {
            Some(db) => db,
            _ => return Err(MethodCallError::UnspecifiedError),
        };
        let db = db.lock().unwrap();

        let mut statement = db
            .prepare("SELECT changes(), last_insert_rowid()")
            .map_err(MethodCallError::from_error)?;
        match statement.next() {
            Ok(sqlite::State::Row) => match (statement.read::<i64>(0), statement.read::<i64>(1)) {
                (Ok(_changes), Ok(rowid)) => Ok(Value::I64(rowid)),
                _ => {
                    warn!("Reading changes for insert didn't return an integer");
                    Ok(Value::Null)
                }
            },
            _ => {
                warn!("Failed to read changes for insert");
                Ok(Value::Null)
            }
        }
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );
        match call.method.as_str() {
            "getPlatformVersion" => Err(MethodCallError::NotImplemented),
            "debugMode" => Err(MethodCallError::NotImplemented),
            "closeDatabase" => {
                let args = from_value::<CloseDatabaseArgs>(&call.args)?;
                self.close_database(args)
            }
            "query" => {
                let args = from_value::<QueryArgs>(&call.args)?;
                self.query(args)
            }
            "insert" => {
                let args = from_value::<InsertArgs>(&call.args)?;
                self.insert(args)
            }
            "update" => {
                let args = from_value::<UpdateArgs>(&call.args)?;
                self.update(args)
            }
            "execute" => {
                let args = from_value::<ExecuteArgs>(&call.args)?;
                self.execute(args)
            }
            "openDatabase" => {
                let args = from_value::<OpenDatabaseArgs>(&call.args)?;
                self.open_database(args)
            }
            "batch" => Err(MethodCallError::NotImplemented),
            "options" => Err(MethodCallError::NotImplemented),
            "getDatabasesPath" => self.get_databases_path(),
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}

fn bind_values_to_statement(
    statement: &mut sqlite::Statement,
    values: &[Value],
) -> Result<(), BindError> {
    let mut index = 1;
    for value in values.iter() {
        match value {
            Value::Null => statement.bind(index, ()).map_err(BindError::SqlError),
            Value::String(s) => statement
                .bind(index, s.as_str())
                .map_err(BindError::SqlError),
            Value::I64(i) => statement.bind(index, *i).map_err(BindError::SqlError),
            Value::I32(i) => statement
                .bind(index, Into::<i64>::into(*i))
                .map_err(BindError::SqlError),
            Value::F64(f) => statement.bind(index, *f).map_err(BindError::SqlError),
            Value::U8List(l) => statement
                .bind::<&[u8]>(index, l)
                .map_err(BindError::SqlError),
            _ => Err(BindError::TypeError),
        }?;
        index += 1;
    }
    Ok(())
}

fn statement_row_to_vec(
    statement: &sqlite::Statement,
    column_count: usize,
) -> sqlite::Result<Vec<Value>> {
    let mut vec = Vec::with_capacity(column_count);

    for i in 0..column_count {
        vec.push(match statement.kind(i) {
            sqlite::Type::Null => Value::Null,
            sqlite::Type::String => Value::String(statement.read(i)?),
            sqlite::Type::Integer => Value::I64(statement.read(i)?),
            sqlite::Type::Float => Value::F64(statement.read(i)?),
            sqlite::Type::Binary => Value::U8List(statement.read(i)?),
        });
    }

    Ok(vec)
}

//fn statement_row_to_map(
//    statement: &sqlite::Statement,
//    column_count: usize,
//) -> sqlite::Result<HashMap<Value, Value>> {
//    let mut map = HashMap::with_capacity(column_count);
//    let column_names = statement.names();
//
//    for i in 0..column_count {
//        map.insert(
//            Value::String(String::from(column_names.get(i).unwrap().clone())),
//            match statement.kind(i) {
//                sqlite::Type::Null => Value::Null,
//                sqlite::Type::String => Value::String(statement.read(i)?),
//                sqlite::Type::Integer => Value::I64(statement.read(i)?),
//                sqlite::Type::Float => Value::F64(statement.read(i)?),
//                sqlite::Type::Binary => Value::U8List(statement.read(i)?),
//            },
//        );
//    }
//
//    Ok(map)
//}

#[derive(Debug)]
enum BindError {
    SqlError(sqlite::Error),
    TypeError,
}

impl std::fmt::Display for BindError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            BindError::TypeError => write!(f, "type error"),
            BindError::SqlError(err) => write!(f, "sql error: {}", err),
        }
    }
}

impl std::error::Error for BindError {}
