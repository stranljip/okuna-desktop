use flutter_engine::plugins::prelude::*;
use log::{debug, error};

use std::sync::Mutex;

use crate::util::event_sink::EventSink;

pub const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "plugins.flutter.io/connectivity";
pub const EVENT_CHANNEL_NAME: &str = "plugins.flutter.io/connectivity_status";

pub struct ConnectivityPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler {
    event_sink: Arc<Mutex<EventSink>>,
}

impl Plugin for ConnectivityPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
        let event_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(EventChannel::new(EVENT_CHANNEL_NAME, event_handler));
    }
}

impl ConnectivityPlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler {
                event_sink: Arc::new(Mutex::new(EventSink::new(EVENT_CHANNEL_NAME.into()))),
            })),
        }
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );
        match call.method.as_str() {
            "check" => Ok(Value::String("wifi".into())),
            "wifiName" => Err(MethodCallError::NotImplemented),
            "wifiBSSID" => Err(MethodCallError::NotImplemented),
            "wifiIPAddress" => Err(MethodCallError::NotImplemented),
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}

impl EventHandler for Handler {
    fn on_listen(&mut self, _args: Value, rt: RuntimeData) -> Result<Value, MethodCallError> {
        debug!("on_listen");
        let mut event_sink = self.event_sink.lock().unwrap();
        event_sink.start_listening(rt)?;
        Ok(Value::Null)
    }

    fn on_cancel(&mut self, _rt: RuntimeData) -> Result<Value, MethodCallError> {
        debug!("on_cancel");
        let mut event_sink = self.event_sink.lock().unwrap();
        event_sink.stop_listening()?;
        Ok(Value::Null)
    }
}
