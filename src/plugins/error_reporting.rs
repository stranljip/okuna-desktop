use flutter_engine::plugins::prelude::*;
use log::error;

pub const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "social.openbook.desktop/error-reporting";

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ReportErrorArgs {
    pub error: String,
    pub stack_trace: String,
}

pub struct ErrorReportingPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler;

impl Plugin for ErrorReportingPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl ErrorReportingPlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler)),
        }
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        match call.method.as_str() {
            "reportError" => {
                let args = from_value::<ReportErrorArgs>(&call.args)?;
                // missing plugins are already reported as warnings by flutter-engine
                if !args.error.starts_with("MissingPluginException") {
                    error!("Caught flutter error: {}\n{}", args.error, args.stack_trace);
                }
                Ok(Value::Null)
            }
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
