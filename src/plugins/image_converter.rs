use flutter_engine::plugins::prelude::*;
use image::GenericImageView;
use log::debug;
use serde::{Deserialize, Serialize};

const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "openspace.social/image_converter";

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
enum TargetFormat {
    JPEG,
    PNG,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
#[serde(rename_all = "camelCase")]
struct ConvertImageArgs {
    image_data: Vec<u8>,
    format: TargetFormat,
}

pub struct ImageConverterPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler;

impl Plugin for ImageConverterPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl ImageConverterPlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler)),
        }
    }
}
impl Handler {
    fn convert_image(&mut self, args: ConvertImageArgs) -> Result<Value, MethodCallError> {
        let image =
            image::load_from_memory(&args.image_data).map_err(MethodCallError::from_error)?;
        let mut buffer = Vec::new();
        match args.format {
            TargetFormat::JPEG => {
                image::jpeg::JPEGEncoder::new_with_quality(&mut buffer, 100)
                    .encode(
                        &image.raw_pixels(),
                        image.width(),
                        image.height(),
                        image.color(),
                    )
                    .map_err(MethodCallError::from_error)?;
            }
            TargetFormat::PNG => {
                image::png::PNGEncoder::new(&mut buffer)
                    .encode(
                        &image.raw_pixels(),
                        image.width(),
                        image.height(),
                        image.color(),
                    )
                    .map_err(MethodCallError::from_error)?;
            }
        }
        Ok(Value::U8List(buffer))
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );

        match call.method.as_str() {
            "convertImage" => {
                let args = from_value::<ConvertImageArgs>(&call.args)?;
                self.convert_image(args)
            }
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
